import tkinter
from time import strftime


class Reloj(object):
    """
    Clase Reloj con el patron Singleton
    :param titulo colorFondo colorFuente Fuente
    Fuente: nombre tamaño opciones
    """
    singleton = None

    def __new__(cls, *args, **kargs):
        if cls.singleton is None:
            cls.singleton = object.__new__(Reloj)
        return cls.singleton

    def __init__(self, titulo, bg, fg, font):
        # Creo la ventana
        self.miVentana = tkinter.Tk()
        self.miVentana.title(titulo)
        self.miVentana.resizable(width=False, height=False)
        #  Label para la hora
        self.miReloj = tkinter.Label()
        self.miReloj['font'] = font
        self.miReloj.pack()
        self.miReloj['bg'] = bg
        self.miReloj['fg'] = fg

    def tic(self):
        self.miReloj['text'] = strftime('%H:%M:%S')

    def tac(self):
        self.tic()
        self.miReloj.after(1000, self.tac)


def main():

    unReloj = Reloj("Reloj V 1.0", "black", "white", "Tahoma 150 bold")
    unReloj.tic()
    unReloj.tac()

    unReloj.miVentana.mainloop()


if __name__ == '__main__':
    main()
